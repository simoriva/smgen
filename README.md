# Synthetic Models of biological systems Generator (SMGen)

## Overview

Several software tools for the simulation and analysis of biochemical reaction networks have been developed in the last decades; however, assessing and comparing their computational performance in executing the typical tasks of Computational Systems Biology can be limited by the lack of a standardized benchmarking approach.
To overcome these limitations, we propose here a novel tool, named SMGen, designed to automatically generate synthetic models of biochemical reaction networks that, by construction, are characterized by both features (e.g. system connectivity, reaction discreteness) and emergent dynamics \textcolor{red}{that do not exhaust all the reactants, a non-trivial behavior that often characterizes} real biochemical networks.
The generation of synthetic models in SMGen is based on the definition of an undirected graph consisting of a single connected component, which generally results in a computationally demanding task.
To avoid any burden in the execution time, SMGen exploits a Main-Worker paradigm to speed up the overall process.
SMGen is also provided with a user-friendly Graphical User Interface that allows the user to easily set up all the parameters required to generate a set of synthetic models with any user-defined number of reactions and species.
We analysed the computational performance of SMGen by generating batches of symmetric and asymmetric Reaction-based Models (RBMs) of increasing size, showing how a different number of reactions and/or species affects the generation time.
Our results show that when the number of reactions is higher than the number of species, SMGen has to identify and correct high numbers of errors during the creation process of the RBMs, a circumstance that increases the overall running time.
Still, SMGen can create synthetic models with 512 species and reactions in less than 7 seconds.

<img src="image/gui.png">


## Installation
```SMGen``` is accessible on ```pypi``` (https://pypi.org/project/smgenerator) and can be installed with ```pip```:

```% pip install smgenenerator```

All package requirements and versions are summarised in ```setup.py``` and are automatically installed with ```SMGen```.


## Usage example
- **single process**:

```% smgenenerator```

- **multi process**:

```% smgenenerator --n_process X```, where X is the number of selected processes which must be >= 3.

**NB**: ```mpi4py``` is required to use the SMGen multi process implementation.


## License
SMGen is licensed under the terms of the  GNU GPL-3 license


## Contact
We're always happy to hear of any suggestions, issues, bug reports, and possible ideas for collaboration.

Simone G. Riva <simo.riva15@gmail.com>


## How to cite SMGen

A detailed description and analyses of SMGen can be found in:
- SG Riva, P Cazzaniga, MS Nobile, S Spolaor, L Rundo, D Besozzi, A Tangherloni: _SMGen: A generator of synthetic models of biochemical reaction networks_, <a href="https://biorxiv.org/cgi/content/short/2021.07.29.454343v1">biorxiv</a>, 2021. doi: 10.1101/2021.07.29.454343


