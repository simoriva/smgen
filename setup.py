import os
import setuptools

with open("README.txt", "r") as fh:
	long_description = fh.read()

setuptools.setup(name = 'smgenerator',
				 version = open("smgenerator/smgenerator/_version.py").readlines()[-1].split()[-1].strip("\"'"),
				 description = 'Synthetic Models of biological systems Generator',
				 author = 'Simone G. Riva',
				 author_email = 'simo.riva15@gmail.com',
				 url = 'https://gitlab.com/sgr34/smgen',
				 keywords = ['Synthetic Models','Reaction-based Models','Biochemical Networks','Systems Biology'],
				 license='LICENCE',
				 long_description=long_description,
				 classifiers = ['Programming Language :: Python :: 3', 'Programming Language :: Python :: 3.5', 'Programming Language :: Python :: 3.6', 'Programming Language :: Python :: 3.7', 'Programming Language :: Python :: 3.8', 'Programming Language :: Python :: 3.9'],
				 python_requires='>=3.5',
				 packages=setuptools.find_packages(where='smgenerator'),
				 py_modules=['smgenerator/smgenerator/__init__', 'smgenerator/smgenerator/_version', 'smgenerator/smgenerator/SMGen_multi', 'smgenerator/smgenerator/SMGen_one', 'smgenerator/smgenerator/cli/__init__', 'smgenerator/smgenerator/cli/smgenerator'],
				 include_package_data=True,
				 package_dir={'smgenerator': 'smgenerator/smgenerator'},
				 package_data={'smgenerator': ['data/load.png', 'data/new.png', 'data/save.png', 'data/exit.png', 'data/gui.ui']},
				 install_requires=['numpy>=1.21.2',
								   'python-libsbml>=5.19.0',
								   'matplotlib>=3.4.3',
								   'networkx>=2.6.3',
								   'PyQt5>=5.15.5'],
				 entry_points = {'console_scripts': ['smgenerator = smgenerator.smgenerator.cli.smgenerator:main']})